#!/bin/bash

VER=`curl -L -I http://colloquy.info/downloads/colloquy-latest.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep '^ETag' | tail -1 | sed 's/ETag: //' | tr -d '\r'`

if [ "x${VER}" != "x" ]; then
    echo ETag: "${VER}"
    echo "${VER}" > current-version
fi

# Update to handle distributed builds
if cmp current-version old-version; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-version old-version
    exit 0
fi
